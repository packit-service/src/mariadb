# This logname can be set in /etc/my.cnf
# by setting the variable "log-error"
# in the [mysqld] section as follows:
#
# [mysqld]
# log-error=@LOG_LOCATION@

@LOG_LOCATION@ {
        create 600 mysql mysql
        notifempty
        daily
        rotate 3
        missingok
        compress
    postrotate
	# just if mariadbd is really running
        if [ -e @PID_FILE_DIR@/@DAEMON_NO_PREFIX@.pid ]
        then
           kill -1 $(<@PID_FILE_DIR@/@DAEMON_NO_PREFIX@.pid)
        fi
    endscript
}
